import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

class Matrix {
    public static void main(String[] args) {
        int arrayRow, arrayCol, minNum, maxNum;
        arrayRow = 0;
        arrayCol = 0;
        minNum = 0;
        maxNum = 0;
        try {
            arrayRow = Integer.parseInt(args[0]);
            arrayCol = Integer.parseInt(args[1]);
            minNum = Integer.parseInt(args[2]);
            maxNum = Integer.parseInt(args[3]);
        } catch (NumberFormatException e) {
            System.out.println(e);
            System.exit(1);
        }
        boolean ascend = false;
        int[][] randomArray = new int[arrayRow][arrayCol];
        int[][] short1Array = new int[arrayRow][arrayCol];
        int[][] short2Array = new int[arrayRow][arrayCol];
        for (int i = 0; i < randomArray.length; i++) {
            for (int j = 0; j < randomArray[i].length; j++) {
                int random =  getRandomBetween(minNum, maxNum);
                randomArray[i][j] = random;
                short1Array[i][j] = random;
                short2Array[i][j] = random;
            }
        }
        for (int i = 0; i < randomArray.length; i++) {
            for (int j = 0; j < randomArray[i].length; j++) {
                System.out.print(randomArray[i][j] + " ");
            }
            System.out.println("");
        }
        int i = 1;
        int temp = 0;
        int value = 0;

        for (int j = 0; j < arrayCol; j++) {

            while (value < (short1Array.length - 1)) {
                if (i == short1Array.length ) {
                    value++;
                    i = value;
                }
                int a = ascend ? value : i;
                int b = ascend ? i : value;
                if (short1Array[a][j] < short1Array[b][j]) {

                    temp = short1Array[value][j];
                    short1Array[value][j] = short1Array[i][j];
                    short1Array[i][j] = temp;

                }
                i++;
            }
            //ascend = (ascend) ? false : true;
            value = 0;
            i = 0;
        }


        /*for (i = 0; i < randomArray.length; i++) {
            for (j = 0; j < randomArray[i].length; j++) {
                if (randomArray[i][1] > randomArray[j][0]) {
                    int firstValue = randomArray[i][1];
                    randomArray[i][1] = randomArray[j][0];
                    randomArray[j][0] = firstValue;
                }

            }

        }*/
        System.out.println("Sorted");
        for (i = 0; i < short1Array.length; i++) {
            for (int j = 0; j < short1Array[i].length; j++) {
                System.out.print(short1Array[i][j] + " ");
            }
            System.out.println("");
        }
        for ( i = 0; i < arrayCol; i++) {
            int j = 0;
            System.out.println("test");
            while (j < (arrayCol - 1)) {
                System.out.println(i);
                if (short2Array[i][j] < short2Array[i][j + 1]) {

                    temp = short2Array[i][j+1];
                    short2Array[i][j+1] = short2Array[i][j];
                    short2Array[i][j] = temp;

                }
                i++;
            }
            //ascend = (ascend) ? false : true;
            value = 0;
            i = 0;
        }
        System.out.println("Sorted");
        for (i = 0; i < short2Array.length; i++) {
            for (int j = 0; j < short2Array[i].length; j++) {
                System.out.print(short2Array[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static int getRandomBetween(int min, int max) {
        Random random = new Random();
        int randomNumber = random.nextInt((max - min) + 1) + min;
        return randomNumber;
    }
}
