import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter first number: ");
        String first = scanner.next();
        System.out.print("Enter parameter(- + * /): ");
        String parameter = scanner.next();
        System.out.print("Enter second number: ");
        String second = scanner.next();
        if (!first.trim().isEmpty() && !parameter.trim().isEmpty() && !second.trim().isEmpty()) {
            int firstNumber = 0;
            int secondNumber = 0;
            try {
                firstNumber = Integer.parseInt(first);
                secondNumber = Integer.parseInt(second);
            } catch (NumberFormatException e) {
                System.out.println(e);
                System.exit(1);
            }
            switch(parameter) {
                case "x":
                case "*":
                    System.out.println(Multiply(firstNumber, secondNumber));
                    break;
                case "/":
                    System.out.println(Divide(firstNumber, secondNumber));
                    break;
                case "+":
                    System.out.println(Add(firstNumber, secondNumber));
                    break;
                case "-":
                    System.out.println(Subtract(firstNumber, secondNumber));
                    break;
                default:
                    System.out.println("no Parameter");

            }
        } else {
            System.out.println("PLS SEND MONEY :,(");
        }
    }
    public static int Multiply(int first, int second) {
        return (first * second);
    }
    public static float Divide(int first, int second) {
        return ((float) first / second);
    }
    public static int Add(int first, int second) {
        return (first + second);
    }
    public static int Subtract(int first, int second) {
        return (first - second);

    }
}
