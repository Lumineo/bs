import java.util.Random;
public class Test implements Runnable {
    public static void main(String[] args) {
        Test test = new Test();
        Thread thread = new Thread(test);
        thread.start();
    }
    public void run() {
        String[][] hiddenRandom = new String[4][4];
        int[][] arrShips = new int[4][4];
        for (int i = 0; i < hiddenRandom.length ; i++) {
            for (int j = 0; j < hiddenRandom.length ; j++ ) {
                hiddenRandom[i][j] = "X";
            }
        }
        boolean shipsDone = true;
        int ships = 0;
        while (shipsDone) {
            for (int i = 0; i < arrShips.length ; i++) {
                for (int j = 0; j < arrShips.length ; j++ ) {
                    int random = getRandomBetween(0, 1);
                    if (random == 1 && ships != 4) {
                        arrShips[i][j] = random;
                        ships++;
                    } else {
                        arrShips[i][j] = 0;
                    }
                }
            }
            if (ships == 4) {
                String output = "";
                int count = 0;
                for (int i = 0; i < arrShips.length ; i++) {
                    for (int j = 0; j < arrShips.length ; j++ ) {
                        output = " " + arrShips[i][j] + " |";
                        System.out.print(output);
                        count += arrShips[i][j];
                        if (count == 4) {
                            shipsDone = false;
                        } else {
                            ships = count;
                        }
                    }
                    System.out.print("\n");
                }
            }
        }
        String output = "" ;
        System.out.println(" A   B   C   D");
        for (int i = 0; i < hiddenRandom.length ; i++) {
            for (int j = 0; j < hiddenRandom.length ; j++ ) {
                output = " " + hiddenRandom[i][j] + " |";
                System.out.print(output);
            }
            System.out.print("\n");

        }

    }
    public static int getRandomBetween(int min, int max) {
        Random random = new Random();
        int randomNumber = random.nextInt((max - min) + 1) + min;
        return randomNumber;
    }

}
