import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
public class RandomArray {
    public static void main(String[] args) {
        int Arraysize = Integer.parseInt(args[0]);
        int minNum = Integer.parseInt(args[1]);
        int maxNum = Integer.parseInt(args[2]);
        int randomArray[] = new int[Arraysize];
        int[] arrHighToLow = new int[Arraysize];
        int[] lowToHighArray = new int[Arraysize];
        String test = "";
        int random = 0;
        for (int i = 0; i < randomArray.length; i++) {
            random = getRandomBetween(minNum, maxNum);
            randomArray[i] = random;
            arrHighToLow[i] = random;
            lowToHighArray[i] = random;

        }
        System.out.println("Random Array " + Arrays.toString(randomArray));
        int temp = 0;
        int i = 1;
        int checker = 0;
        while (checker < (lowToHighArray.length - 1)) {
            if (i == lowToHighArray.length) {
                checker++;
                i = checker;

            }
            if (lowToHighArray[checker] > lowToHighArray[i]) {

                temp = lowToHighArray[checker];
                lowToHighArray[checker] = lowToHighArray[i];
                lowToHighArray[i] = temp;

            }
            i++;
        }
        i = 1;
        checker = 0;
        temp = 0;
        while (checker < (arrHighToLow.length - 1)) {
            if (i == arrHighToLow.length) {
                checker++;
                i = checker;

            }
            if (arrHighToLow[checker] < arrHighToLow[i]) {
                temp = arrHighToLow[checker];
                arrHighToLow[checker] = arrHighToLow[i];
                arrHighToLow[i] = temp;

            }
            i++;
        }
        System.out.println("Low to high: " + Arrays.toString(lowToHighArray));
        System.out.println("High to Low: " + Arrays.toString(arrHighToLow));

    }
    public static int getRandomBetween(int min, int max) {
        Random random = new Random();
        int randomNumber = random.nextInt((max - min) + 1) + min;
        return randomNumber;
    }
}
