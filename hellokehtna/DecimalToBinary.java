public class DecimalToBinary {
    public static void main(String[] args) {
        int decimal = 0;

        try {
            decimal = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.out.println(e);
            System.exit(1);
        }
        String binary = "";

        for (int i = 0; i < 8; i++) {
            if (decimal % 2 == 1) {
                binary = "1" + binary;
            } else {
                binary = "0" + binary;
            }
            decimal = decimal / 2;
        }
        System.out.println(binary);
    }
}
